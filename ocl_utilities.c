#include <stdio.h>
#include <stdlib.h>

#if defined(__APPLE__)
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif
/* <sys/time.h> needed for time measurements.  */
#include <sys/time.h>
#include <string.h>

#ifdef ENERGY_COUNTERS
  #include "papi_interface.h"
#endif

/* Return the OpenCL error string for a given error number.
 */
const char *opencl_error_string(cl_int error)
{
	int errorCount;
	int index;

	static const char *errorString[] = {
		[CL_SUCCESS] = "CL_SUCCESS",
		[-CL_DEVICE_NOT_FOUND] = "CL_DEVICE_NOT_FOUND",
		[-CL_DEVICE_NOT_AVAILABLE] = "CL_DEVICE_NOT_AVAILABLE",
		[-CL_COMPILER_NOT_AVAILABLE] = "CL_COMPILER_NOT_AVAILABLE",
		[-CL_MEM_OBJECT_ALLOCATION_FAILURE] =
			"CL_MEM_OBJECT_ALLOCATION_FAILURE",
		[-CL_OUT_OF_RESOURCES] = "CL_OUT_OF_RESOURCES",
		[-CL_OUT_OF_HOST_MEMORY] = "CL_OUT_OF_HOST_MEMORY",
		[-CL_PROFILING_INFO_NOT_AVAILABLE] =
			"CL_PROFILING_INFO_NOT_AVAILABLE",
		[-CL_MEM_COPY_OVERLAP] = "CL_MEM_COPY_OVERLAP",
		[-CL_IMAGE_FORMAT_MISMATCH] = "CL_IMAGE_FORMAT_MISMATCH",
		[-CL_IMAGE_FORMAT_NOT_SUPPORTED] =
			"CL_IMAGE_FORMAT_NOT_SUPPORTED",
		[-CL_BUILD_PROGRAM_FAILURE] = "CL_BUILD_PROGRAM_FAILURE",
		[-CL_MAP_FAILURE] = "CL_MAP_FAILURE",
		[-CL_INVALID_VALUE] = "CL_INVALID_VALUE",
		[-CL_INVALID_DEVICE_TYPE] = "CL_INVALID_DEVICE_TYPE",
		[-CL_INVALID_PLATFORM] = "CL_INVALID_PLATFORM",
		[-CL_INVALID_DEVICE] = "CL_INVALID_DEVICE",
		[-CL_INVALID_CONTEXT] = "CL_INVALID_CONTEXT",
		[-CL_INVALID_QUEUE_PROPERTIES] = "CL_INVALID_QUEUE_PROPERTIES",
		[-CL_INVALID_COMMAND_QUEUE] = "CL_INVALID_COMMAND_QUEUE",
		[-CL_INVALID_HOST_PTR] = "CL_INVALID_HOST_PTR",
		[-CL_INVALID_MEM_OBJECT] = "CL_INVALID_MEM_OBJECT",
		[-CL_INVALID_IMAGE_FORMAT_DESCRIPTOR] =
			"CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
		[-CL_INVALID_IMAGE_SIZE] = "CL_INVALID_IMAGE_SIZE",
		[-CL_INVALID_SAMPLER] = "CL_INVALID_SAMPLER",
		[-CL_INVALID_BINARY] = "CL_INVALID_BINARY",
		[-CL_INVALID_BUILD_OPTIONS] = "CL_INVALID_BUILD_OPTIONS",
		[-CL_INVALID_PROGRAM] = "CL_INVALID_PROGRAM",
		[-CL_INVALID_PROGRAM_EXECUTABLE] =
			"CL_INVALID_PROGRAM_EXECUTABLE",
		[-CL_INVALID_KERNEL_NAME] = "CL_INVALID_KERNEL_NAME",
		[-CL_INVALID_KERNEL_DEFINITION] =
			"CL_INVALID_KERNEL_DEFINITION",
		[-CL_INVALID_KERNEL] = "CL_INVALID_KERNEL",
		[-CL_INVALID_ARG_INDEX] = "CL_INVALID_ARG_INDEX",
		[-CL_INVALID_ARG_VALUE] = "CL_INVALID_ARG_VALUE",
		[-CL_INVALID_ARG_SIZE] = "CL_INVALID_ARG_SIZE",
		[-CL_INVALID_KERNEL_ARGS] = "CL_INVALID_KERNEL_ARGS",
		[-CL_INVALID_WORK_DIMENSION] = "CL_INVALID_WORK_DIMENSION",
		[-CL_INVALID_WORK_GROUP_SIZE] = "CL_INVALID_WORK_GROUP_SIZE",
		[-CL_INVALID_WORK_ITEM_SIZE] = "CL_INVALID_WORK_ITEM_SIZE",
		[-CL_INVALID_GLOBAL_OFFSET] = "CL_INVALID_GLOBAL_OFFSET",
		[-CL_INVALID_EVENT_WAIT_LIST] = "CL_INVALID_EVENT_WAIT_LIST",
		[-CL_INVALID_EVENT] = "CL_INVALID_EVENT",
		[-CL_INVALID_OPERATION] = "CL_INVALID_OPERATION",
		[-CL_INVALID_GL_OBJECT] = "CL_INVALID_GL_OBJECT",
		[-CL_INVALID_BUFFER_SIZE] = "CL_INVALID_BUFFER_SIZE",
		[-CL_INVALID_MIP_LEVEL] = "CL_INVALID_MIP_LEVEL",
		[-CL_INVALID_GLOBAL_WORK_SIZE] = "CL_INVALID_GLOBAL_WORK_SIZE",
		[-CL_INVALID_PROPERTY] = "CL_INVALID_PROPERTY"
	};

	errorCount = sizeof(errorString) / sizeof(errorString[0]);
	index = -error;

	return (index >= 0 && index < errorCount) ?
		errorString[index] : "Unspecified Error";
}


void  opencl_device_type_to_string(cl_device_type device, char* desc)
{
	strcpy(desc,"");
	if (CL_DEVICE_TYPE_CPU == (device & CL_DEVICE_TYPE_CPU)) {
		strcat (desc,"CPU_Device ");
	}

	if (CL_DEVICE_TYPE_GPU == (device & CL_DEVICE_TYPE_GPU)) {
		strcat (desc,"GPU_DEVICE ");
	}

	if (CL_DEVICE_TYPE_ACCELERATOR == (device & CL_DEVICE_TYPE_ACCELERATOR)) {
		strcat (desc,"ACCELERATOR ");
	}

	if (CL_DEVICE_TYPE_DEFAULT == (device & CL_DEVICE_TYPE_DEFAULT)) {
		strcat (desc,"DEFAULT");
	}
}
/* Find a GPU or a CPU associated with the first available platform.
 * If use_gpu is set, then this function first tries to look for a GPU
 * in the first available platform.
 * If this fails or if use_gpu is not set, then it tries to use the CPU.
 */
cl_device_id opencl_create_device(int use_gpu)
{
	cl_platform_id platform;
	cl_device_id dev;
	int err;

	err = clGetPlatformIDs(1, &platform, NULL);
	if (err < 0) {
		fprintf(stderr, "Error %s while looking for a platform.\n",
				opencl_error_string(err));
		exit(1);
	}

	err = CL_DEVICE_NOT_FOUND;
	if (use_gpu)
		err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev,
				NULL);
	if (err == CL_DEVICE_NOT_FOUND)
		err = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev,
				NULL);
	if (err < 0) {
		fprintf(stderr, "Error %s while looking for a device.\n",
				opencl_error_string(err));
		exit(1);
	}
	return dev;
}

/* Create an OpenCL program from a string and compile it.
 */
cl_program opencl_build_program_from_string(cl_context ctx, cl_device_id dev,
	const char *program_source, size_t program_size,
	const char *opencl_options)
{
	int err;
	cl_program program;
	char *program_log;
	size_t log_size;

	program = clCreateProgramWithSource(ctx, 1,
			&program_source, &program_size, &err);
	if (err < 0) {
		fprintf(stderr, "Could not create the program\n");
		exit(1);
	}
	err = clBuildProgram(program, 0, NULL, opencl_options, NULL, NULL);
	if (err < 0) {
		fprintf(stderr, "Could not build the program.\n");
		clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG, 0,
				NULL, &log_size);
		program_log = (char *) malloc(log_size + 1);
		program_log[log_size] = '\0';
		clGetProgramBuildInfo(program, dev, CL_PROGRAM_BUILD_LOG,
				log_size + 1, program_log, NULL);
		fprintf(stderr, "%s\n", program_log);
		free(program_log);
		exit(1);
	}
	return program;
}

/* Create an OpenCL program from a source file and compile it.
 */
cl_program opencl_build_program_from_file(cl_context ctx, cl_device_id dev,
	const char* filename, const char* opencl_options)
{
	cl_program program;
	FILE *program_file;
	char *program_source;
	size_t program_size, read;

	program_file = fopen(filename, "r");
	if (program_file == NULL) {
		fprintf(stderr, "Could not find the source file.\n");
		exit(1);
	}
	fseek(program_file, 0, SEEK_END);
	program_size = ftell(program_file);
	rewind(program_file);
	program_source = (char *) malloc(program_size + 1);
	program_source[program_size] = '\0';
	read = fread(program_source, sizeof(char), program_size, program_file);
	if (read != program_size) {
		fprintf(stderr, "Error while reading the kernel.\n");
		exit(1);
	}
	fclose(program_file);

	program = opencl_build_program_from_string(ctx, dev, program_source,
						program_size, opencl_options);
	free(program_source);

	return program;
}

cl_device_type get_opencl_device_type(cl_device_id device) {
  cl_device_type device_type;
  int err = clGetDeviceInfo (device, CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);
  if (err < 0) {
    printf ("Error getting device type:  %s",opencl_error_string(err));
    exit(-1);
  }
  return device_type;
}

void print_device_info(cl_device_id device) {
  #define NAME_SIZE 100
  char device_name[NAME_SIZE];
  cl_device_type device_type;
  int err = clGetDeviceInfo (device, CL_DEVICE_NAME, NAME_SIZE, device_name, NULL);
  if (err < 0) {
    printf ("Error getting device name:  %s",opencl_error_string(err));
  }
  /* 
   err = clGetDeviceInfo (device, CL_DEVICE_TYPE, sizeof(cl_device_type), &device_type, NULL);
  if (err < 0) {
    printf ("Error getting device type:  %s",opencl_error_string(err));
  }
  */
  device_type = get_opencl_device_type(device);
  char type_str[200];
  opencl_device_type_to_string(device_type, type_str);
  printf ("%s,Type: %s\n", device_name, type_str);
}

void print_platform_info(cl_platform_id platform) {
  #define NAME_SIZE 100
  char platform_name[NAME_SIZE];
  int err = clGetPlatformInfo (platform, CL_PLATFORM_NAME, NAME_SIZE, platform_name, NULL);
  if (err < 0) {
    printf ("Error getting platform info:  %s",opencl_error_string(err));
   }	 
  printf ("Platform:%s\n", platform_name);
}

/* 
 * Select the OpenCL device to run the generated code on.
 * The devic is choosen based on the device_type.
 * 0 : run on a CPU device.
 * 1 : Run on an accelerator (Xoeon Phi).
 * 2 : Run on a GPU (Nvidia Graphics card).
 */
cl_device_id  opencl_enumrate_devices(int device_type)
{
  #define MAX_PLATFORMS 10
  #define MAX_DEVICES 10

  cl_device_id  all_devices[MAX_DEVICES*MAX_PLATFORMS];

  printf ("Enumerating all devices\n");
  cl_platform_id platforms[MAX_PLATFORMS];
  cl_uint num_platforms = 0;
  cl_device_id devices[MAX_DEVICES];
  cl_uint num_devices     = 0;  /* Devices per platform   */
  cl_uint tot_num_devices = 0;  /* Total number of devices*/
  cl_uint device_idx;
  cl_uint platform_idx;
  int dev_choice = -1;;
  cl_device_type cl_dev_type;
  int err;

  switch (device_type) 
  {
    case 0:
      cl_dev_type = CL_DEVICE_TYPE_CPU;
      break;
    case 1:
      cl_dev_type = CL_DEVICE_TYPE_ACCELERATOR;
      break;
    case 2:
      cl_dev_type = CL_DEVICE_TYPE_GPU;
      break;
    default:
      fprintf(stderr , "Error , unidentified device type \n");
      exit(-1);
  }

  err = clGetPlatformIDs(MAX_PLATFORMS, platforms, &num_platforms);
  if (err < 0) {
    fprintf(stderr, "Error %s while looking for a platform.\n", opencl_error_string(err));
    exit(-1);
  }

  if (num_platforms < 1) {
    fprintf(stderr, "Error: no platforms found while looking for a platform.\n");
    exit(-1);
  }

  printf ("Number of platforms: %i\n", num_platforms);
  /* For each platform find all the posssible devices: */
  for (platform_idx = 0; platform_idx < num_platforms; platform_idx++) {
    print_platform_info(platforms[platform_idx]);
    err = clGetDeviceIDs(platforms[platform_idx], CL_DEVICE_TYPE_ALL, MAX_DEVICES, devices, &num_devices);
    if (err < 0) {
      fprintf(stderr, "Error %s while looking for a device while enumeration.\n", opencl_error_string(err));
      num_devices = 0;
      continue;
    }
    printf ("Number of devices in platform: %i\n", num_devices);
    for (device_idx = 0; device_idx < num_devices; device_idx++) {
      all_devices[tot_num_devices] = devices[device_idx];
      printf ("%i) ", tot_num_devices);
      print_device_info(devices[device_idx]);
      /* Check if the device is of the required type */
      /* if we don't have a matching device yet, save the device */
      if (dev_choice == -1) {
        if (cl_dev_type == get_opencl_device_type(devices[device_idx])) {
          dev_choice = tot_num_devices;
        }
      }
      tot_num_devices++;
    }
    printf("\n");
  }

  if (tot_num_devices == 0) {
    printf ("Error, no devices found in your system\n");
    exit(0);
  }

  if (dev_choice == -1) {
    printf ("Requested device type not found in system. Exiting!\n");
    exit(-1);
  } else {
    printf ("Running on device: %i\n", dev_choice);
    return all_devices[dev_choice];
  }
}

/* Return current time (number of seconds since the Epoch).  */
double ppcg_get_time()
{
	struct timeval Tp;
	int stat;

	stat = gettimeofday (&Tp, NULL);
	if (stat != 0)
		fprintf(stderr, "Error return from gettimeofday: %d", stat);
	return Tp.tv_sec + Tp.tv_usec * 1.0e-6;
}

#ifdef ENERGY_COUNTERS
// start and stop can be called multiple times.
void start_energy_monitoring(char* sess_name, BOOL rapl, BOOL cpu, BOOL mic, BOOL gpu) {
    START(NULL, sess_name, rapl, cpu, mic, gpu);
}

void stop_energy_monitoring() {
  STOP();
}
#endif
