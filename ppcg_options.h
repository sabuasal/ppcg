#ifndef PPCG_OPTIONS_H
#define PPCG_OPTIONS_H

#include <isl/arg.h>

struct ppcg_debug_options {
	int dump_schedule_constraints;
	int dump_schedule;
	int dump_sizes;
	int verbose;
};

struct ppcg_analytics_options {
	int dump_memory_access;
};

struct ppcg_options {
	struct ppcg_debug_options *debug;
	struct ppcg_analytics_options *analytics;

	int scale_tile_loops;
	int wrap;

	char *ctx;
	char *sizes;

	int tile_size;

	/* Take advantage of private memory. */
	int use_private_memory;

	/* Take advantage of shared memory. */
	int use_shared_memory;

	/* Maximal amount of shared memory. */
	int max_shared_memory;

	/* The target we generate code for. */
	int target;

	/* Generate OpenMP macros (C target only). */
	int openmp;

	/* Linearize all device arrays. */
	int linearize_device_arrays;

	/* Allow live range to be reordered. */
	int live_range_reordering;

	/* Options to pass to the OpenCL compiler.  */
	char *opencl_compiler_options;
	/* Prefer GPU device over CPU. */
        /* Deprecated */
        /* int opencl_use_gpu;*/
        /* Device type preferred (CPU 0, Acclelerator 1, GPU 2)*/
	int opencl_device_type;
	/* Number of files to include. */
	/* Instrument the generated OpenCL code to print the kernels
	 * execution time + data copy time in stdout (kernel compilation
	 * time is excluded from the measured time).
	 */
	int opencl_measure_time;
	/* Instruments the generated openc code with calls to start and stop
	 * Energy measurement.*/
	int opencl_measure_energy;
        /* Add pauses before and after the start and stop of energy measurements
         * Duration in seconds.*/
        int opencl_energy_pause;
	int opencl_n_include_file;
	/* Files to include. */
	const char **opencl_include_files;
	/* Embed OpenCL kernel code in host code. */
	int opencl_embed_kernel_code;

};

ISL_ARG_DECL(ppcg_debug_options, struct ppcg_debug_options,
	ppcg_debug_options_args)
ISL_ARG_DECL(ppcg_analytics_options, struct ppcg_analytics_options,
	ppcg_analytic_options_args)
ISL_ARG_DECL(ppcg_options, struct ppcg_options, ppcg_options_args)

#define		PPCG_TARGET_C		0
#define		PPCG_TARGET_CUDA	1
#define		PPCG_TARGET_OPENCL      2

#endif
